# Impression 3D

N’ayant pas eu le temps de coder une pièce car j’ai eu une semaine très chargée (3 hôpitaux en 1 semaine). Je n’ai pas suivi les directives qu’il fallait suivre mais je suis quand même arrivé au bon résultat (presque). J’expliquerai dans les lignes suivantes ce qu’il fallait faire et ce que j’ai fait par la suite.

Pour cette semaine, nous avons dû utiliser les imprimantes 3D pour imprimer les flexlinks que nous avons codés la semaine précédente, en utilisant le logiciel [*Prusa Slicer*](https://help.prusa3d.com/fr/article/telecharger-prusaslicer_2220).

## Paramétrage

Il est important de choisir le bon modèle d’imprimante 3D sur le logiciel. Pour ce faire, il faut ajouter le modèle d’imprimante utilisé au Fablab dans l’onglet "Réglages de l'imprimante" : l’Original Prusa i3 MK3S & MK3S.

Ensuite, nous pouvons ajouter une bordure à notre objet pour augmenter son adhérence au support, afin qu’il ne se décolle pas. Pour ce faire, dans l’onglet "Réglages d’impression", il faut aller dans "Jupes et bordures de Plateau", et cocher la case "Bordure".

Il faut préciser le type de filament utilisé dans le logiciel pour s’assurer du bon déroulement de l’impression. Chaque filament a des propriétés différentes. Au Fablab, le filament utilisé est du PLA. Dans le logiciel, dans l'onglet "Réglages de filament", il faut ajouter *"prusma Pla"*.

Enfin, pour importer les fichiers STL, fichier lisible par le logiciel d’impression, il faut aller dans "Fichier > Importer > Importer STL". Si on n'a pas de fichier STL, c'est parce que l'on a oublié d'exporter notre fichier dans le bon format sur OpenSCAD. Pour résoudre ce problème, il suffit d'aller dans "Fichier > Exporter > Exporter comme STL".

Dans mon cas, Prusa m'a affiché un message m'indiquant que mes données étaient en pouces et me demandant si je voulais les convertir en millimètres. J'ai cliqué sur "non", gardant mes mesures en pouces, mais je recommande fortement d'appuyer sur "oui" pour les convertir en millimètres.

Enfin, on ajoute notre objet sur le plateau, et on clique sur "Découper > Exporter le fichier gcode". On peut alors enregistrer le fichier gcode dans la carte SD. Le fichier Gcode est un format de fichier qui est lu par l'imprimante 3D et qui lui indique tous les mouvements qu'elle doit faire pour réaliser notre pièce.

## Utilisation de l'imprimante 3D

La carte SD sur laquelle se trouve le fichier gcode doit être insérée dans le côté gauche de l'imprimante 3D. Ensuite, on allume l'imprimante, on choisit notre fichier en tournant et en appuyant sur le joystick de l'imprimante, et on clique sur "Lancer l’impression". Avant de lancer l'impression, il est important de vérifier si c'est le bon type de filament, s'il en reste assez, s'il n'y a pas de nœuds et que le filament est bien tendu. De plus, il faut nettoyer le plateau à l'alcool ou à l'acétone pour éviter toutes taches de gras qui pourraient nuire à l'adhérence.

Quand l'impression démarre, il est important de rester devant l'impression pour s'assurer que tout se passe bien. Si il y a un bug, que l'imprimante est mal paramétrée, que la pièce se décolle, il faut absolument arrêter l'impression et recommencer, car une imprimante n'est pas capable d'imprimer dans le vide et on évite ainsi de gaspiller du plastique.

### La bonne manière d'imprimer

Pour connaître la bonne valeur de nos paramètres de nos flexlinks, par exemple le rayon des trous (car le devoir indiquait que notre flexlink devait être adapté à un Lego), il faut imprimer une barrette avec des trous de taille différentes qui augmentent de manière progressive. Une fois la bonne valeur trouvée, il faut retourner sur OpenSCAD, insérer les bonnes valeurs dans notre code, et imprimer entièrement notre pièce.

#### La manière d'un droitier qui a deux mains gauches

N'ayant pas codé d’objet comprenant un gradient, une échelle de paramètre, j'ai commencé par obtenir ceci : 

![img1](../images/satelitte3d.jpg)

Ce qui m'a donné l'objet (que j'ai cassé) tout en bas sur cette photo :

![img2](../images/Imp3D.jpg)

En cours d'opération, je suis passé de l'objet satelite à l'objet curve et en allant sur Prusa j'ai eu un objet très petit sans avoir vu que j'étais en pouces : 

![img3](../images/curve1.3D.jpg)

Ici, mon objet fait 0.18 sur l'axe X (inch), soit 4.5 mm, donc trop petit. J'ai donc décidé de changer la valeur et de mettre 1.4 inch, soit 35 mm, et d'essayer une impression. J'ai obtenu un objet (le 2ème sur la photo postée un peu plus haut) qui était presque correct en termes de taille globale mais pour la taille des trous ça n'allait pas.

j'ai changé le paramètre (```distance(```) dans le code en bas de page ici [code du curve](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/donovan.crousse/-/blob/main/docs/fabzero-modules/module02.md)
Avec les valeurs:
|0.4  |0.5  |0,6|
|------|--------|-------| 
<<<<<<< HEAD
![](../images/curve-d0.4-1.4-3D.jpg)
Pour gagner du temps, j'ai enlever mes objets à la moitié du temps de l'impression et j'ai utilisé 2 imprimantes ne meme temp. Il s'avère que le bon objet était celui ayant une valeur de 0.5
![](../images/3D-final.jpg)
=======
![img4](../images/curve-d0.4-1.4-3D.jpg)

>>>>>>> 87b26971a27cdfce33e3bca4b5dd56e0d96a0337


Pour gagner du temps, j'ai enlevé mes objets à la moitié du temps de l'impression et j'ai utilisé 2 imprimantes en même temps. Il s'est avéré que le bon objet était celui ayant une valeur de 0.5.

![img5](../images/3D-final.jpg)

J'étais persuadé d'avoir fini, mais même si mon objet était de la bonne taille, il n'était pas flexible. Pour régler cela, j'ai divisé par 2 la différence entre le paramètre `rayon_f` et le paramètre `width_f` :

![img6](../images/fin.jpg)

J'ai obtenu cet objet sur Prusa :

![img7](../images/bon-curve.jpg)

Qui est flexible, comme le montre cette vidéo : 

[je faisais des vidéos de magie](https://youtube.com/shorts/_Ow6f05Ynf8)







