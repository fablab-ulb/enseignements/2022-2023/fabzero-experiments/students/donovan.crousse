# 5. Dynamique de groupe et projet final
## Arbre des problèmes (en collaboration avec [Matthew](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/))
![d](../images/arbprblm.jpg)

Le plancton est extrêmement important pour tous les écosystèmes marins, cependant, il existe peu de connaissances sur les différentes variétés et les déplacements de ces micro-organismes. Par conséquent, il est difficile de mesurer l'impact des activités humaines sur le plancton et de mettre en place des stratégies de conservation. Cette lacune d'informations est principalement causée par un manque d'équipement et de navires spécialisés pour étudier les vastes zones concernées.

# arbre des solutions (en collaboration avec [Matthew](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/))
![d](../images/arbprblm2.jpg)


Le Planktoscope a été conçu pour recueillir davantage d'informations sur le plancton. Cet outil est facile à utiliser et permet d'obtenir un grand nombre d'images d'échantillons de plancton. Le modèle du Planktoscope est disponible en ligne, et il est déjà utilisé par une grande communauté de personnes et d'organisations. L'objectif est d'utiliser cet outil pour collecter plus d'informations sur les écosystèmes marins et évaluer l'impact des activités humaines sur ces écosystèmes. De plus, le Planktoscope devrait faciliter la mise en place de stratégies de conservation.
#Formation des groupes 
## Mon objet
![h](../images/md5.jpg)


La semaine concernant la création de groupe, il nous a été demandé de prendre un objet symbolisant une problématique à laquelle nous sommes sensibles. Étant en biologie, je me suis dit que quelque chose en rapport avec la nature, le recyclage, la pollution, serait dans le thème. J'ai pensé à prendre une plante, mais pour certaines raisons logistiques, ce n'était pas une bonne idée. Ayant faim, je suis venue en cours avec 2 bananes, et je me suis rendu compte que ces fruits, en plus de résoudre le problème de logistique, symbolisaient parfaitement mon attrait pour le recyclage. Faire du compost avec les déchets organiques et éviter les emballages plastiques permet de réduire le nombre de déchets produits par individu.

## La formation des groupes


Pour constituer les groupes, nous avons commencé par former un cercle et chacun a présenté son objet. Ensuite, nous nous sommes dirigés vers les personnes qui avaient des objets similaires au nôtre. Nous avons finalement formé des groupes de 4 ou 5 personnes avec celles qui avaient des objets les plus proches. Dans mon cas, j'ai été regroupé avec des étudiants qui avaient également choisi des objets liés aux problèmes de recyclage/produit polluant.

Je me suis mit en groupe avec le beau [Louis Devroye](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.devroye/) le magnifique [Christophe Ory](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/christophe.ory/)
Le somptueux [Alexandre Hallemans](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/) et avec la charmante [Kawtar Zaouia](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Kawtar.zaouia/) ( qui nous a rejoins par la suite)

# La thématique 

À la suite de nos échanges, nous avons convenu de différentes thématiques et après avoir exprimé nos avis, il a été décidé que nous allions travailler sur la bioaccumulation du plastique dans les organismes aquatiques de la Meuse. De manière imagée, nous allons essayer de faire en sorte qu'une truite évite de manger du plastique. Pour nous mettre d'accord, nous avons dû nous battre dans un combat de sumo, mais la première règle du Fight Club est qu'il ne faut pas en parler. Plus sérieusement, afin de décider de cette thématique, nous avons tous pris des post-it et noté les thèmes sur lesquels nous voulions travailler en relation avec les objets que nous avions choisis.
![g](../images/md5.1.jpg)


Par la suite, j'ai été envoyé en territoire ennemi afin de savoir si les autres étudiants pouvaient éclairer notre lanterne et nous apporter d'autres idées.

![g](../images/md5.2.jpg)


## roles, technique et méthode de décision au sein d'un groupe
### roles
A chaque réunion du groupe, il est conseillé de nommer :  

**Un animateur**, qui s'assure que chacun ait l'occasion de s'exprimer, et que les interventions restent pertinentes pour le groupe.  

**Chronos**,Une personne responsable de la gestion du temps, qui veille à ce que chaque problème soit traité dans des délais raisonnables.  

**Un scribe**, Un secrétaire, qui prend note des points les plus importants de la réunion.  
 
 **Elon Musk**, la personne qui s'assure que la communication en dehors des réunions se passe correctement.    
 ### méthode de décison pour les nuls (éditions 2023)
 Si le groupe est indécis ou que plusieurs idée se présentent et qu'individuellement et/ou collectivement  il est compliqué de choisir, voici différentes méthodes qui peuvent aider :   

 **Voie majoritaire** Comme lorqu'on vote pour un parti politique c'est l'idée qui garde le plus de voix qui sera gardé.  

 **vote pondéré** une méthode de décision qui permet à chaque membre du groupe de démontrer son enthousiasme envers chaque idée (par exemple : démontrer en même temps de 1 à 5 avec ses doigts son enthousiame envers une idée).  

 **tirage au sort** pile ou face, choisir un chiffre au hasard ect.  
 
 **Ne pas prendre de décision** Il arrive des fois qu'une ou l'emsemble des idées soit encore trop juvéniles et qu'il vaut mieux laisser du temps à ces derniers de germer afin de voir si elles étaient vraiment bonne   

Ps: il est important de noter que :   
la communication est indispensable au sein d'un groupe.  
Il faut manger 5 fruits et légumes par jour.  
Emprunter de l'argent coute aussi de l'argent.  
Panzani aussi.
