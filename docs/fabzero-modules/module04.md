# 4. Outil Fab Lab sélectionné

Cette semaine, j'ai dû choisir et travailler sur l'un des outils suivants du Fab Lab : Microcontrôleur, CNC, Laser Cutter et Imprimante 3D. Mon choix s'est porté sur le Microcontrôleur.

J'ai reçu le YD-RP2040, un microcontrôleur chinois, et il a été demandé de pouvoir paramétrer une LED RGB, un contrôleur de température et d'humidité, ainsi que de prendre des données avec un capteur plus avancé.

La photo suivante montre les différentes parties de la carte, y compris les entrées et les sorties :
![mr1](../images/pinout.jpg)

## Les différents logiciels
Pour insérer du code dans le microcontrôleur, il faut qu'il puisse communiquer avec le PC. Cela se fait à l'aide du logiciel Arduino IDE (en C++) ou via Thonny (en Python). J'ai donc subdivisé cette partie en deux sous-parties distinctes :

### Utilisation de l'Arduino IDE
Il est important de suivre les étapes qui nous ont été fournies sur ce site [étapes à suivre](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/languages/c/). En résumé, il faut choisir le bon port USB et la bonne "cible" afin d'assurer une bonne connexion entre le PC et le microcontrôleur.

### Utilisation de Thonny
Pour utiliser Thonny, il faut suivre les étapes suivantes [étapes à suivre](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/languages/python/).

## petit poisson dans un océan 
### Paramétrer la lumière RGB
Le devoir que je devais réaliser consistait à écrire un code qui me permettait de créer une couleur en paramétrant différentes valeurs de la LED RGB (allant de 0 à 255 pour le rouge, le vert et le bleu). Pour ce faire, j'ai utilisé Thonny car j'ai des bases en Python. À ce moment-là, ma santé mentale n'était pas encore dégradée. Sur la photo suivante et via le code que j'ai écrit, j'ai eu une **erreur** :

![mr](../images/mrc3.jpg)

Je ne comprenais pas d'où ça venait. Je l'ai réécrit. Puis, je l'ai ré-réécrit. Puis, je l'ai ré-ré-réécrit et l'erreur persistait. Ma santé mentale commençait à se dégrader. 
Je me suis dit que cela venait de mon code et que je voulais surement faire trop compliqué. Donc je suis passé à quelque chose de plus simple et je voulais simplement faire clignoter la lumière en sortie 1 et là c'est le drame :
![mr2](../images/mrc5.jpg)
Bien que le code soit mal écrit (je n'avais pas défini le chiffre de sortie pour la lumière (1)), une erreur persistait à chaque fois. Il m'a fallu plus de 6 heures pour comprendre qu'il fallait installer les bibliothèques de code sur la carte. En MicroPython, il est nécessaire de faire cela, car la carte, lorsqu'elle lit le code ``import...(from)``, va chercher la librairie dans son espace de stockage et non dans le PC. 

Après avoir compris cela, j'ai écrit le code suivant: 

```
import time
from neopixel import Neopixel
from machine import Pin

BUTTON_PIN = 24
button = Pin(BUTTON_PIN, Pin.IN, Pin.PULL_UP)
 
numpix = 1
pixels = Neopixel(numpix, 0, 23, "GRB")
color = (0,0,0)
 
pixels.brightness(10)
pixels.set_pixel(0, color)
pixels.show()
 
red = int(input("Entrez une valeur pour le rouge (0-255) : "))
green = int(input("Entrez une valeur pour le vert (0-255) : "))
blue = int(input("Entrez une valeur pour le bleu (0-255) : "))
color = (red,green,blue)
 
pixels.brightness(255)
pixels.set_pixel(0, color)
pixels.show()

boolbutton = False
while True:
    if not button.value():
        if boolbutton : 
            red = int(input("Entrez une valeur pour le rouge (0-255) : "))
            green = int(input("Entrez une valeur pour le vert (0-255) : "))
            blue = int(input("Entrez une valeur pour le bleu (0-255) : "))
            color = (red,green,blue)
            pixels.set_pixel(0, color)
            pixels.show()
            boolbutton = False
        else:
            color=(0,0,0)
            pixels.set_pixel(0, color)
            pixels.show()
            boolbutton = True
            time.sleep(1)
```

Ce code demande à l'utilisateur de rentrer des valeurs pour les différents paramètres cités plus haut et, si on appuie sur le bouton situé en sortie 24, il éteint la lumière. Pour la rallumer, il faut rappuyer sur le bouton et insérer de nouvelles valeurs. Ce qui me donne le résultat dans cette [vidéo](https://youtube.com/shorts/4HoOtI0T3zw).

### Paramétrer le capteur de température et d'humidité

Celui-ci était plus simple dans le code car je suis passé par Arduino IPE, mais plus difficile dans le montage. Je n'en ai pas fait la photo (cependant, on peut le voir dans la vidéo qui viendra par la suite), mais il faut ajouter 4 câbles entre le capteur et le microcontrôleur en passant par ce que j'appelle "la palette blanche".

![cap](../images/DHT20.jpg)

Une fois le montage réalisé, il faut passer par l'onglet **board additionnelle** de l'application et ajouter la bibliothèque DHT20. Par la suite, Il faut aller dans l'onglet **dossier-->example-->Dht20-->plotter**. Il faut aussi remplacer, dans le code, ``DHT.begin()`` par:

```
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();
```

Puis lancer le code. J'ai pris la température et l'humidité ambiante de mon frigo [ici](https://youtube.com/shorts/T7cG2iFTcyM).

### Descente aux enfers ou le capteur à particules

Pour celui-ci, j'ai décidé de passer par Arduino IPE, car c'était plus simple d'utilisation. Je n'ai pas reçu le nom de ce capteur. Donc, je me suis un peu perdu sur Internet avant de trouver ce site [capteur](https://wiki.dfrobot.com/PM2.5_laser_dust_sens).
En important ce code sur l'application:

```
 //******************************
 //*Abstract: Read value of PM1,PM2.5 and PM10 of air quality
 //
 //*Version：V3.1
 //*Author：Zuyang @ HUST
 //*Modified by Cain for Arduino Hardware Serial port compatibility
 //*Date：March.25.2016
 //******************************
#include <Arduino.h>
#define LENG 31   //0x42 + 31 bytes equal to 32 bytes
unsigned char buf[LENG];

int PM01Value=0;          //define PM1.0 value of the air detector module
int PM2_5Value=0;         //define PM2.5 value of the air detector module
int PM10Value=0;         //define PM10 value of the air detector module


void setup()
{
  Serial.begin(9600);   //use serial0
  Serial.setTimeout(1500);    //set the Timeout to 1500ms, longer than the data transmission periodic time of the sensor

}

void loop()
{
  if(Serial.find(0x42)){    //start to read when detect 0x42
    Serial.readBytes(buf,LENG);

    if(buf[0] == 0x4d){
      if(checkValue(buf,LENG)){
        PM01Value=transmitPM01(buf); //count PM1.0 value of the air detector module
        PM2_5Value=transmitPM2_5(buf);//count PM2.5 value of the air detector module
        PM10Value=transmitPM10(buf); //count PM10 value of the air detector module
      }
    }
  }

  static unsigned long OledTimer=millis();
    if (millis() - OledTimer >=1000)
    {
      OledTimer=millis();

      Serial.print("PM1.0: ");
      Serial.print(PM01Value);
      Serial.println("  ug/m3");

      Serial.print("PM2.5: ");
      Serial.print(PM2_5Value);
      Serial.println("  ug/m3");

      Serial.print("PM1 0: ");
      Serial.print(PM10Value);
      Serial.println("  ug/m3");
      Serial.println();
    }

}
char checkValue(unsigned char *thebuf, char leng)
{
  char receiveflag=0;
  int receiveSum=0;

  for(int i=0; i<(leng-2); i++){
  receiveSum=receiveSum+thebuf[i];
  }
  receiveSum=receiveSum + 0x42;

  if(receiveSum == ((thebuf[leng-2]<<8)+thebuf[leng-1]))  //check the serial data
  {
    receiveSum = 0;
    receiveflag = 1;
  }
  return receiveflag;
}

int transmitPM01(unsigned char *thebuf)
{
  int PM01Val;
  PM01Val=((thebuf[3]<<8) + thebuf[4]); //count PM1.0 value of the air detector module
  return PM01Val;
}

//transmit PM Value to PC
int transmitPM2_5(unsigned char *thebuf)
{
  int PM2_5Val;
  PM2_5Val=((thebuf[5]<<8) + thebuf[6]);//count PM2.5 value of the air detector module
  return PM2_5Val; 
}

//transmit PM Value to PC
int transmitPM10(unsigned char *thebuf)
{
  int PM10Val;
  PM10Val=((thebuf[7]<<8) + thebuf[8]); //count PM10 value of the air detector module
  return PM10Val;
}
``` 

Mon microcontrôleur se déconnectait automatiquement. Ce schéma s'est reproduit pendant 3 heures avant que je demande pourquoi à un contact qui m'a informé que selon lui : 

-**tu te sers du RX/TX sur un Arduino, tu ne peux pas le conserver sur ton port USB parce que La connexion pour le code et pour le RX/TX est la même**

N'ayant toujours pas compris, je lui demande ce que cela signifie et il me répond : 

-**Bah si t'es déjà au téléphone**
 **Et que je t'appelle**
 **. La ligne est occupée**

-**Donc si j'essaie de t'envoyer du code, mais que t'es occupé à parler du beau temps avec le capteur, bah tu m'écouteras pas et je vais juste partir**
 **Même principe**

Il m'a dit que pour régler ce problème, je devais passer par un protocole I2C, chose que j'ai essayé de comprendre (j'ai compris que c'était une autre manière de communiquer) et d'installer sur Arduino IPE.

**Résultat** : Je compte prendre rendez-vous chez un psy car j'en ai conclut que tout les informaticiens était masochistes.
