# 2. Conception Assistée par Ordinateur (CAO)


Lors de cette deuxième semaine, notre objectif a été de réaliser un ensemble de pièces compatibles lego sous le modèle des [_flex links_](https://www.compliantmechanisms.byu.edu/flexlinks). Le but est d'utiliser un logiciel de CAO (Conception Assitée par Ordinateur) pour designer quelques pièces pour former un kit compatible lego de _flex links_.  
Pour nous pouvions utiliser [OpenSCAD](https://www.openscad.org/) ou [FreeCAD](https://www.freecadweb.org/). J'ai préféré utiliser OpenSCAD car ce logiciel me convenait mieux.

## Pièces réalisées

Pour l'ensemble des pièces suivantes il est préférable de commencer par décrire les contour de la pièce et ensuite décrit la forme des trous. Finalement, réaliser une différence de ces deux descriptions (```difference()```), permet d'avancer  progressivement. Cette  méthode est  conseille. car  Avancer de cette manière permet d'augmenter progressivement la complexité du programme sans augmenter particulièrement la difficulté de son écriture. 

 les pièces  décrites ont un code permettant de changer les paramètres au début du programme afin de pouvoir modifier les paramètres facilement et créer de nouvelles pièces à partir de celles déjà décrites.

OpenSCAD est un logiciel informatique et réalise donc des approximations pour les nombres décimaux. Cela peut causer de légers bugs d'affichage. En particulier Lorsque l'on fait une différence entre deux parties possèdant une (ou plusieurs) même dimension. Pour corriger ce problème j'ai légèrement agrandi certaines épaisseurs de +epsilon et translaté suivant l'axe z de -espilon/2. Epsilon est une variable que j'ai défini au début de mon code.

# Licenses

Mettre une creative license sur notre travail permet aux autres d’utiliser notre travail. Ce qui est tout l’intérêt de cette documentation.

Il en existe plusieurs options.


| Options|Significations|
|---    |:-:    |
|   BY    | Credit must be given to the creator      |      
|   SA    | Adaptations must be shared under the same terms      |      
|   NC    | Only noncommercial uses of the work are permitted      |
|   ND    | No derivatives or adaptations of the work are permitted      |     




Il existe donc différents types de licenses :

-CC BY
-CC BY-SA
-CC BY-NC
-CC BY-NC-SA
-CC BY-ND
-CC BY-NC-ND
Pour CC-licensing nos codes, il suffit de choisir le type de license que l’on veut et communiquer clairement cette license dans notre travail (il faut inclure le lien de la license).

Voici comment j’ai ajouté ma license sur mon fichier OpenSCAD.
```
// File : Curve.scad

//Author : Donovan Crousse

// Date : 01 mars 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```


### Pièce 1 : satellite

![](../images/satelitte.jpg)


Pour réaliser cette pièce,  le code suivant  a été écrit:
```
*
    File    : propulsion.scad
    Author  : Donovan Crousse
    Date    : 01/03/2023
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 1;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair
theta = 45;       //angle d'inclinaison des parties sur les cotes

//dimensions de la partie centrale
length_c = 1.5;
delta_y = 0.1;
delta_x = 0.25;

//dimension de la partie "satellite"
length_s = 4;
width_s = 0.1;
N_s = 2;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 3;      //nombre de trous

union(){

//premiere partie en angle
translate([0,-(cos(theta)*(dist*(N-1)+ext_r))-delta_y,0])
rotate([0,0,theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//partie centrale
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x,-ext_r,0])  //decalage a l'extremite de la partie precedente
difference(){
    union(){
        cylinder(th,ext_r,ext_r);
        translate([0,-ext_r,0])
        cube([length_c+ext_r*2-diff,width,th]);
        translate([length_c+ext_r*2-diff,0,0]) //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);
    }
    union(){ //creux
        translate([0,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre a une extremite

        translate([ext_r*2-diff/2,0,0])
        union(){
            translate([0,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur

            translate([0,-ext_r+diff,-epsilon/2])
            cube([length_c-ext_r*2,width-diff*2,th+epsilon]);

            translate([length_c-ext_r*2,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur
        }

        translate([length_c+ext_r*2-diff,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cynlindre a une extremite
    }
}


//deuxieme partie en angle
translate([sin(theta)*(dist*(N-1)+ext_r)+(length_c+ext_r*2-diff)-delta_x,-ext_r,0])
rotate([0,0,-theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}


//partie "satellite"
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x+(length_c+ext_r*2-diff)/2+width_s/2,-(dist*(N_s-1)+ext_r+length_s+width),0])
rotate([0,0,90])
union(){
    difference(){
        union(){
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
            translate([0,-ext_r,0])
            cube([dist*(N_s-1),width,th]);
            translate([dist*(N_s-1),0,0])    //decalage a l'extremite du pave
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        }
        union(){  //ensemble de N cylindres pour realiser les trous
            for (i=[0:N_s]){
                translate([dist*i,0,-epsilon/2])
                cylinder(th+epsilon,ext_r-diff,ext_r-diff);
            }
        }
        }

        //fil
        translate([dist*(N_s-1)+ext_r,-width_s/2,0])  //decalage a l'extremite de la partie precedente
        cube([length_s,width_s,th]);
    }

}
```
### pièce 2: curve

![](../images/curve-original.jpg)

pour réaliser cette pièce,le code suivant à été écrit: 

```
$fn = 100;        //nombre de face des cylindres
th = 0.5;         //epaisseur de la pièce
width = 0.6;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair

//dimensions du fil (ensemble de 2 cylindres
rayon_f = 1.5;
width_f = 0.1;

//dimensions pour les trous
dist = 0.5; //distance entre les trous
N = 2;      //nombre de trous

union(){

//premiere partie
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([dist*(N-1)+ext_r,-rayon_f+width_f/2,0])  //decalage a l'extremite de la partie precedente
difference(){
    cylinder(th,rayon_f,rayon_f);
    translate([0,0,-epsilon/2])
    cylinder(th+epsilon,rayon_f-width_f,rayon_f-width_f);
    translate([-rayon_f,-rayon_f,-epsilon/2])
    cube([rayon_f,rayon_f*2,th+epsilon]);
    translate([-epsilon/2,-rayon_f,-epsilon/2])
    cube([rayon_f+epsilon,rayon_f,th+epsilon]);
}

//deuxieme partie
translate([dist*(N-1)+ext_r+rayon_f-width_f/2,-rayon_f-(dist*(N-1)+ext_r)+width_f/2,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([-ext_r,0,0])
        cube([width,dist*(N-1),th]);
        translate([0,dist*(N-1),0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([0,dist*i,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}
```

