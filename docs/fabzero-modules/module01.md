# 1. Gestion de projet et documentation

cette semaine j'ai appris à travailler sur les bases de la documentation de projet et sur les différents outils en ligne de git lab

## Initiation

"J'ai commencé par installer le terminal/console gitbash ![](../images/git-bash-photo.jpg)et on peut trouver une cheatsheet pour ce logiciel sur ce lien [git](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet) et je me suis initié au language de programation markdown.Sur ce lien on peut trouver une cheatsheet pour le markdown [markdown](https://www.markdownguide.org/cheat-sheet/). J'ai aussi installer le logiciel Visual Code studio [Vs](https://code.visualstudio.com/) qui est un éditeur de texte et qui me permet de visualiser le rendu de mon code.  De plus, j'ai du créer une paire de clé ssh entre mon espace gitlab en ligne et mon pc. Dans le cadre de ce cours le travail en local est favorisé car cela à un impact environemmental moins important. Par la suite, j'ai du éditer ma présentation

##clé ssh et cloner projet

Il faut ouvrir git bash et insérer la commande **ssh-keygen -t ed25519** ce qui donne une clé ssh public et privé.![](../images/clé-pub-pri.jpg)

```
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/doria/.ssh/id_rsa): git_fab
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in git_fab
Your public key has been saved in git_fab.pub
The key fingerprint is:
SHA256:KzOSEg/4mBzVazwB76umuWYZys+ISULCudaTuf0PgFk doria@LAPTOP-OL4EA4RG
The key's randomart image is:
+---[RSA 3072]----+
|   .             |
|    +            |
|   . E           |
|..o * o          |
|o=oo B  S        |
|+=+++.+  .       |
|*===+.+..        |
|**+o=. +.        |
|==== .....       |
+----[SHA256]-----+
```
 la console nous informe de son lieu de stockage. ![](../images/lieux-stockage-clé.jpg)
Il faut ajouter la clé public sur git lab en cliquant en haut à droite sur mon profil puis aller dans préférence et cliquer sur ajouter une clé ssh.![](00/images/lieux-dépot-lien-clé.jpg)
Une fois ceci fait,il faut retouner sur la console  et insérer la commande **ssh -T git@gitlab.com** afin de se connecter à git lab via la clé ssh et assurer une connexion local-en ligne ![](../images/connexion-loca-ligne.jpg)
Puis, il faut retouner sur gitlab et copier l'adresse https de mon projet (**https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/donovan.crousse.git**)
![](../images/adrs.jpg)
Par la suite, sur la console, j'introduis la commande **git clone** suivi de l'adresse que j'ai copié afin de pouvoir cloner les fichiers en ligne sur mon pc ![](../images/illustration-cmd.jpg)

## modification de la présentation
 Une fois les étapes précédentes effectuer. les devoirs implique que je doive changer ma présentation en parlant de moi et en ajoutant une photo me représentant
 il faut compresser la photo car elle est très lourde (plusieur mégaoctets)
 Pour ce faire, je suis passé par l'éditeur de photo gimp.
Ensuite, après avoir introduit ma modification sur la console **git commit** 
```
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   fichier1
        new file:   fichier2
        modified:   fichier3
```
Finalement pour envoyer vos modifications sur la plateforme vous pouvez utiliser la commande suivante :
```
$ git push origin master






